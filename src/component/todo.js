import React from "react";

class Todo extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            tasks: [],
            input: ""
        };
    }
    render(){
        return (
            <div>
                <h1>Todo Tasks</h1>
                <ul>
                    {
                        this.state.tasks.map((task, i)=>
                            <li key={i}>
                                
                                {task}
                                <button data-index = {i} onClick={this.deleteTask}>X</button>
                            </li>
                        )
                    }           
                </ul>
                <div>
                    <input onChange = {this.handleChange} value={this.state.input}/>
                    <button onClick = {this.addTask}>Add</button>
                </div>
                Number of Tasks = {this.state.tasks.length}
            </div>
        );
    }
    handleChange = (event) => {
        this.setState({
            input: event.target.value
        });
    }
    addTask = () => {
        this.setState(state =>({
            tasks:[...state.tasks, state.input],
            input: ""
        }));
    }
    deleteTask = (event) => {
        const index = event.target.dataset.index;
        this.setState(state => {
            let tasks = [...state.tasks];
            tasks.splice(index, 1);
            return {
                tasks: tasks
            };

        });
        
    }

}

export default Todo;